#!/usr/bin/env python3 -O
#     SimpleFractal is a script that generates simple png images of the Mandelbrot set.
#     Copyright (C) 2019, 2020  Christian Calderon
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For comments and questions, email the author at calderonchristian73@gmail.com
from __future__ import annotations
import re
import png
import sys
import array
from argparse import ArgumentParser, ArgumentTypeError, Namespace
from typing import List, Tuple, NamedTuple
from dataclasses import dataclass
from multiprocessing import Pool, cpu_count
from math import ceil, log2, log, log1p
from cmath import sqrt as csqrt
from os import urandom


class Color(NamedTuple):
    r: int
    g: int
    b: int

    def to_hex(self) -> str:
        return '{:02X}{:02X}{:02X}'.format(self.r, self.g, self.b)

    @staticmethod
    def from_hex(s: str) -> Color:
        m = re.match(COLOR_PATTERN, s)
        if m is None:
            raise ValueError('Incorrect value for hex formatted color: {}'.format(s))

        return Color(int(s[0:2], 16),
                     int(s[2:4], 16),
                     int(s[4:6], 16))


class NormalizedColor(NamedTuple):
    r: float
    g: float
    b: float

    @staticmethod
    def from_color(c: Color) -> NormalizedColor:
        return NormalizedColor(c.r/255.0, c.g/255.0, c.b/255.0)


VERSION = '2.0'
DEFAULT_BIT_DEPTH = 8
DEFAULT_PIXELS_TYPECODE = 'B'
DEFAULT_BANDED_BAIL_RADIUS = 2.0
DEFAULT_SMOOTH_BAIL_RADIUS = 200.0
DEFAULT_CENTER = -1.25066 + 0.02012j
DEFAULT_ZOOM = 1500000
DEFAULT_TRIALS = 10000
DEFAULT_WIDTH = 640
DEFAULT_HEIGHT = 640
DEFAULT_INNER_COLOR = Color(0, 0, 0)
DEFAULT_OUTER_COLOR_PALETTE = [
    Color(181, 203, 4),
    Color(69, 93, 224),
    Color(17, 185, 245),
    Color(234, 93, 83),
    Color(34, 247, 227),
]
COLOR_PATTERN = r'^[0-9a-f]{6}$'
COMPLEX_NUM_PATTERN = r'^([+-]?\d+\.\d+)([+-]\d+\.\d+)j$'


@dataclass
class Bounds:
    top: float
    left: float
    real_step: float
    imaginary_step: float


def positive_int(s: str) -> int:
    n = int(s)
    if n <= 0:
        raise ArgumentTypeError('argument must be an int greater than zero!')
    return n


def color(s: str) -> Color:
    s = s.lower()
    m = re.match(COLOR_PATTERN, s)
    if m is None:
        raise ArgumentTypeError('color must be in hex format like ffffff!')
    r = int(s[0:2], 16)
    g = int(s[2:4], 16)
    b = int(s[4:], 16)
    return Color(r, g, b)


def color_palette(s: str) -> List[Color]:
    s = s.lower()
    colors = s.split(',')
    return [color(c) for c in colors]


def complex_num(s: str) -> complex:
    m = re.match(COMPLEX_NUM_PATTERN, s)
    if m is None:
        raise ArgumentTypeError('invalid format for complex number!')
    r = float(m.group(1))
    i = float(m.group(2))
    return complex(r, i)


def has_png_ext(s: str):
    if not s.endswith('.png'):
        raise ArgumentTypeError('PNG filename must end with the \'.png\' extension')

    try:
        image_file = open(s, 'wb')
    except OSError as exc:
        raise ArgumentTypeError('Unable to open image file.') from exc

    return image_file


def parse_args(args: List[str]) -> Namespace:
    parser = ArgumentParser(
        description='Generates images of the Mandelbrot set.'
    )
    parser.add_argument('-o', '--output',
                        help=('Specify a filename ending with .png for the image. '
                              'If this option is not provided, the image will be named '
                              'mandelbrot-W-H.png, with W and H replaced with the pixel width and height.'),
                        type=has_png_ext,
                        default=None)
    parser.add_argument('--version',
                        help='Print the version and exit.',
                        action='store_true',
                        default=False)
    parser.add_argument('-W', '--width',
                        help='Image width in pixels.',
                        type=positive_int,
                        default=DEFAULT_WIDTH)
    parser.add_argument('-H', '--height',
                        help='Image height in pixels.',
                        type=positive_int,
                        default=DEFAULT_HEIGHT)
    parser.add_argument('-T', '--trials',
                        help='The number of trials for checking each point.',
                        type=positive_int,
                        default=DEFAULT_TRIALS)
    parser.add_argument('-I', '--inner-color',
                        help='Color (hex) used for points inside the Mandelbrot set.',
                        type=color,
                        default=DEFAULT_INNER_COLOR)
    parser.add_argument('-O', '--outer-color-palette',
                        help='Colors (hex, comma separated) used for points outside the Mandelbrot set.',
                        type=color_palette,
                        default=DEFAULT_OUTER_COLOR_PALETTE)
    parser.add_argument('-R', '--random-outer-palette',
                        help='Use a palette of random colors',
                        type=positive_int,
                        default=0)
    parser.add_argument('-C', '--center',
                        help=('Complex number to set as the center of the image. '
                              'For negative real part you have to do -C=-whatever.'),
                        type=complex_num,
                        default=DEFAULT_CENTER)
    parser.add_argument('-Z', '--zoom',
                        help=('Amount to \'zoom\' the image. '
                              'This value is used to compute the bounds of points '
                              'that are checked for the image by dividing the image '
                              'height and width and centering on the provided center.'),
                        type=positive_int,
                        default=DEFAULT_ZOOM)

    parser.set_defaults(strategy=None)
    commands = parser.add_subparsers(help='Sets the coloring strategy.', dest='strategy')
    banded = commands.add_parser('banded', help='Use the simple banded coloring strategy.')
    banded.set_defaults(func=mandelbrot_row_banded, radius=DEFAULT_BANDED_BAIL_RADIUS)
    smooth = commands.add_parser('smooth', help='Use a smooth coloring strategy.')
    smooth.set_defaults(func=mandelbrot_row_smooth, radius=DEFAULT_SMOOTH_BAIL_RADIUS)

    return parser.parse_args(args)


def mandelbrot_test(z: complex, trials: int, radius: float) -> Tuple[complex, int]:
    # cardioid test
    if abs(1 - csqrt(1 - 4*z)) < 1:
        return z, trials

    c = z
    last = trials - 1
    for i in range(trials):
        if abs(z) > radius:
            return z, i
        if i != last:
            z = z*z + c

    return z, trials


def mandelbrot_row_banded(
        z_i: float,
        z_r: float,
        r_step: float,
        r_width: int,
        colors: List[Color],
        trials: int,
        radius: float) -> array.ArrayType:
    """Computes a single row of pixels for the image."""

    result = array.array(DEFAULT_PIXELS_TYPECODE, bytes(r_width))
    palette_length = len(colors) - 1  # Because the last color is only used for pixels inside the set.
    z = complex(z_r, z_i)
    for index in range(r_width):
        final_z, final_trials = mandelbrot_test(z, trials, radius)

        if final_trials == trials:
            result[index] = palette_length
        else:
            result[index] = final_trials % palette_length

        z += r_step

    return result


def interp(a: float, b: float, p: float) -> float:
    return a + p*(b - a)


def mandelbrot_row_smooth(
        z_i: float,
        z_r: float,
        r_step: float,
        r_width: int,
        colors: List[Color],
        trials: int,
        radius: float) -> array.ArrayType:
    pixel_mask = (1 << DEFAULT_BIT_DEPTH) - 1
    result = array.array(DEFAULT_PIXELS_TYPECODE, bytes(3*r_width))
    palette_length = len(colors) - 1  # Because the last color is only used for pixels inside the set.
    inner_r, inner_g, inner_b = colors[-1]
    inner_r = int(pixel_mask * inner_r)
    inner_g = int(pixel_mask * inner_g)
    inner_b = int(pixel_mask * inner_b)
    z = complex(z_r, z_i)
    for index in range(0, 3*r_width, 3):
        final_z, final_trials = mandelbrot_test(z, trials, radius)

        if final_trials == trials:
            result[index] = inner_r
            result[index+1] = inner_g
            result[index+2] = inner_b
        else:
            smooth_count = log1p(final_trials + 1 - log2(log(abs(final_z), radius)))
            start_color_index = int(smooth_count) % palette_length
            end_color_index = (start_color_index + 1) % palette_length
            percent = smooth_count % 1.0
            start_color = colors[start_color_index]
            end_color = colors[end_color_index]
            result[index] = int(pixel_mask*interp(start_color.r, end_color.r, percent))
            result[index+1] = int(pixel_mask*interp(start_color.g, end_color.g, percent))
            result[index+2] = int(pixel_mask*interp(start_color.b, end_color.b, percent))

        z += r_step

    return result


def compute_boundaries(center: complex, width: int, height: int, zoom: int) -> Bounds:
    bound_width = width / zoom
    bound_height = height / zoom
    center_r = center.real
    center_i = center.imag

    return Bounds(
        top=center_i + bound_height / 2,
        left=center_r - bound_width / 2,
        real_step=bound_width / (width - 1),
        imaginary_step=bound_height / (height - 1)
    )


def random_palette(length: int) -> List[Color]:
    return [Color(*urandom(3)) for _ in range(length)]


def main(argv: List[str]) -> int:
    args = parse_args(argv)

    if args.strategy is None:
        if args.version:
            print('SimpleFractal v{}'.format(VERSION))
            print('Copyright (C) 2019, 2020 Christian Calderon')
            print('License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.')
            print('This is free software: you are free to change and redistribute it.')
            print('There is NO WARRANTY, to the extent permitted by law.')
            return 0
        else:
            print('No coloring strategy selected, try the -h flag to see usage information.')
            return 1

    b = compute_boundaries(
        args.center,
        args.width,
        args.height,
        args.zoom
    )
    p = Pool()
    p_chunk = int(ceil(args.height / cpu_count()))
    # p_chunk is the approximate number of jobs to assign to each process in the pool.

    if args.random_outer_palette:
        outer_palette = random_palette(args.random_outer_palette)
    else:
        outer_palette = args.outer_color_palette

    colors = outer_palette + [args.inner_color]

    # noinspection PyUnreachableCode
    if __debug__:
        print('\n'.join(c.to_hex() for c in colors))

    if args.strategy == 'banded':
        image = png.Writer(width=args.width,
                           height=args.height,
                           palette=colors,
                           bitdepth=DEFAULT_BIT_DEPTH,
                           greyscale=False,
                           compression=9)
    else:
        colors = [NormalizedColor.from_color(c) for c in colors]
        image = png.Writer(width=args.width,
                           height=args.height,
                           bitdepth=DEFAULT_BIT_DEPTH,
                           greyscale=False,
                           compression=9)

    row_args = []
    for step_count in range(args.height):
        z_i = b.top - step_count * b.imaginary_step
        row_args.append((
            z_i,
            b.left,
            b.real_step,
            args.width,
            colors,
            args.trials,
            args.radius
        ))

    pixel_rows = p.starmap(args.func, row_args, p_chunk)
    p.close()

    if args.output is None:
        filename = 'mandelbrot-{}-{}.png'.format(args.width, args.height)
        with open(filename, 'wb') as png_file:
            image.write(png_file, pixel_rows)
    else:
        image.write(args.output, pixel_rows)
        args.output.close()

    p.join()
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
