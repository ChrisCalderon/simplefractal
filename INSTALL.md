# Dependency Installation Instructions
To use this script, you must have at least Python 3.7 installed, as well as pip.

## Installing Python and pip on Debian Sid, Ubuntu 19.10, and derived distros
On these distros, installing Python and pip is as simple as running these commands in your favorite terminal emulator:

```
sudo apt update
sudo apt install python3 python3-pip
```

## Installing Python on an older Linux distros
For older distros, you will have to download the python source and compile it yourself. The source code for Python can be found on [python.org][1]. Detailed instructions for this vary depending on your distro, and describing them all is beyond the scope of this guide.

## Installing Python and pip on Windows and macOS
For these systems, I recommend installing Python and pip via the executable installers found on [python.org][1]. For Windows users, make sure to click on the option in the installer that says "Add Python to PATH".

## Installing PyPNG
Once Python and pip are both installed, open up a console (Terminal app on macOS, Command Prompt on Windows) and run this command: `pip3 install pypng`

[1]: https://www.python.org/downloads/release/python-375/
