# SimpleFractal

A script that produces PNG images of the Mandelbrot set.

Help output:

```
usage: SimpleFractal.py [-h] [-o OUTPUT] [--version] [-W WIDTH] [-H HEIGHT] [-T TRIALS]
                        [-I INNER_COLOR] [-O OUTER_COLOR_PALETTE]
                        [-R RANDOM_OUTER_PALETTE] [-C CENTER] [-Z ZOOM]
                        {banded,smooth} ...

Generates images of the Mandelbrot set.

positional arguments:
  {banded,smooth}       Sets the coloring strategy.
    banded              Use the simple banded coloring strategy.
    smooth              Use a smooth coloring strategy.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Specify a filename ending with .png for the image. If this
                        option is not provided, the image will be named mandelbrot-
                        W-H.png, with W and H replaced with the pixel width and height.
  --version             Print the version and exit.
  -W WIDTH, --width WIDTH
                        Image width in pixels.
  -H HEIGHT, --height HEIGHT
                        Image height in pixels.
  -T TRIALS, --trials TRIALS
                        The number of trials for checking each point.
  -I INNER_COLOR, --inner-color INNER_COLOR
                        Color (hex) used for points inside the Mandelbrot set.
  -O OUTER_COLOR_PALETTE, --outer-color-palette OUTER_COLOR_PALETTE
                        Colors (hex, comma separated) used for points outside the
                        Mandelbrot set.
  -R RANDOM_OUTER_PALETTE, --random-outer-palette RANDOM_OUTER_PALETTE
                        Use a palette of random colors
  -C CENTER, --center CENTER
                        Complex number to set as the center of the image. For negative
                        real part you have to do -C=-whatever.
  -Z ZOOM, --zoom ZOOM  Amount to 'zoom' the image. This value is used to compute the
                        bounds of points that are checked for the image by dividing the
                        image height and width and centering on the provided center.
```

Here is the default image produced:

![640x640 Mandelbrot PNG](mandelbrot-640-640.png)
